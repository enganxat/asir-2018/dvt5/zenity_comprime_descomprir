#!/bin/bash
dir(){
DIR=`zenity --file-selection --directory --title="Seleccione directorio"`
}
comprimir(){
FICHERO=0
while [ $FICHERO = 0 ]
        do
        FICHERO=`zenity --file-selection --title="Seleccione un fichero para comprimir" \
        --separator=" " \
        --multiple`
        case $? in
                 0)zenity --info --text="\"$FICHERO\" seleccionado.";;
                 1)zenity --info --text="No ha seleccionado ningún fichero"
                        FICHERO=0;;
                -1)zenity --info --text="Ha ocurrido un error inesperado"
                        FICHERO=0;;
        esac
        done
                NOM=`zenity --entry --title="Introduzca nombre del archivo"`
                zenity --info --text="Comprimiendo"
                tar -czvf $NOM.tar.gz $FICHERO
                zenity --info --text="La Compresión ha resultado con éxito"
}
dir
comprimir

